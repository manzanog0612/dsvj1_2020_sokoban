#ifndef ELEMENTS_H
#define ELEMENTS_H

#include "raylib.h"

#include "elements_properties.h"
#include "configurations/configurations.h"

using namespace game;
using namespace elements_properties;
using namespace configurations;

namespace game
{
	namespace elements
	{
		struct Player
		{
			Controls control;
			short row;
			short column;
			bool moving;
			Vector2 pos;
		};
	}
}
#endif //ELEMENTS_H