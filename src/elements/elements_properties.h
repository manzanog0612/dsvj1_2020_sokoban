#ifndef ELEMENTS_PROPERTIES_H
#define ELEMENTS_PROPERTIES_H

namespace game
{
	namespace elements_properties
	{
		enum class COLOR
		{
			_GRAY = 1, _DARKGRAY, _YELLOW, _GOLD, _ORANGE, _PINK, _RED, _MAROON, _GREEN, _DARKGREEN, _SKYBLUE,
			_BLUE, _DARKBLUE, _VIOLET, _DARKPURPLE, _BEIGE, _BROWN, _DARKBROWN, _WHITE, _BLACK, _MAGENTA, _RAYWHITE
		};

		struct Controls
		{
			int up;
			int down;
			int left;
			int right;
		};
	}
}

#endif // ELEMENTS_PROPERTIES_H