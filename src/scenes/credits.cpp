#include "scenes/credits.h"

#include "raylib.h"
#include <iostream>

#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "configurations/configurations.h"
#include "resourses/textures.h"
#include "resourses/audio.h"
#include "resourses/events.h"

using namespace game;
using namespace global_vars;
using namespace global_drawing_vars;
using namespace configurations;
using namespace textures;
using namespace audio;
using namespace events;

namespace game
{
	namespace credits
	{
		void initialization()
		{
			loadCreditsTextures();

			const short fontSize = static_cast<int>(20 * drawScaleY);

			creditsText[firstLine].text = "Audio";
			creditsText[secondLine].text = "From the page freesound.org:";
			creditsText[thirdLine].text = "https://freesound.org";
			creditsText[fourthLine].text = "From youtube:";
			creditsText[fifthLine].text = "https://www.youtube.com";
			creditsText[sixthLine].text = "Textures";
			creditsText[seventhLine].text = "From hiclipart:";
			creditsText[eighthLine].text = "https://www.hiclipart.com";
			creditsText[ninethLine].text = "From hipwallpaper:";
			creditsText[tenthLine].text = "https://hipwallpaper.com";
			creditsText[eleventhLine].text = "All the rest of the assets were made by Guillermina Manzano"; 
			creditsText[twelfthLine].text = "Game desing and development";
			creditsText[thirteenthLine].text = "by Guillermina Manzano";

			pressEnter.text = "Press enter to go back to menu";

			for (short i = firstLine; i <= thirteenthLine; i++)
			{
				if (i == firstLine)
					creditsText[i].posY = static_cast<int>(screenHeight / 7.0f);
				else 
					creditsText[i].posY = static_cast<int>(creditsText[i - 1].posY + fontSize * 2.1f);

			}
		}

		void input()
		{
			if (IsKeyPressed(KEY_ENTER))
				returnToMenu = true;

			if (IsKeyPressed(KEY_M))
				mute = !mute;
		}

		void update()
		{
			if (returnToMenu)
			{
				currentPlaceInGame = PLACEINGAME::MENU;
				returnToMenu = false;
			}

			if (!mute)
				UpdateMusicStream(menuStream);
		}

		void draw()
		{
			ClearBackground(BLACK);

			DrawTextureEx(menuBackground.texture, { 0.0f, 0.0f }, 0.0f, 1.0f, WHITE);

			const short fontSize = static_cast<int>(20.0f * drawScaleY);
			const Color color = WHITE;
			pressEnter.posY = static_cast<int>(screenLimit.down - pressEnter.fontSize * 2.0f);
			pressEnter.fontSize = static_cast<int>(25.0f * drawScaleY);

			DrawText(creditsText[firstLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[firstLine].text, fontSize + 15)) / 2, creditsText[firstLine].posY, fontSize + 15, MAGENTA);
			DrawText(creditsText[secondLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[secondLine].text, fontSize + 4)) / 2, creditsText[secondLine].posY, fontSize + 4, YELLOW);
			DrawText(creditsText[thirdLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[thirdLine].text, fontSize)) / 2, creditsText[thirdLine].posY, fontSize, color);
			DrawText(creditsText[fourthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[fourthLine].text, fontSize + 4)) / 2, creditsText[fourthLine].posY, fontSize + 4, YELLOW);
			DrawText(creditsText[fifthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[fifthLine].text, fontSize)) / 2, creditsText[fifthLine].posY, fontSize, color);
			DrawText(creditsText[sixthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[sixthLine].text, fontSize + 15)) / 2, creditsText[sixthLine].posY, fontSize + 15, MAGENTA);
			DrawText(creditsText[seventhLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[seventhLine].text, fontSize + 4)) / 2, creditsText[seventhLine].posY, fontSize + 4, YELLOW);
			DrawText(creditsText[eighthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[eighthLine].text, fontSize)) / 2, creditsText[eighthLine].posY, fontSize, color);
			DrawText(creditsText[ninethLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[ninethLine].text, fontSize + 4)) / 2, creditsText[ninethLine].posY, fontSize + 4, YELLOW);
			DrawText(creditsText[tenthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[tenthLine].text, fontSize)) / 2, creditsText[tenthLine].posY, fontSize, color);
			DrawText(creditsText[eleventhLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[eleventhLine].text, fontSize)) / 2, creditsText[eleventhLine].posY, fontSize, color);
			DrawText(creditsText[twelfthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[twelfthLine].text, fontSize + 15)) / 2, creditsText[twelfthLine].posY, fontSize + 15, MAGENTA);
			DrawText(creditsText[thirteenthLine].text, (static_cast<int>(screenWidth) - MeasureText(creditsText[thirteenthLine].text, fontSize)) / 2, creditsText[thirteenthLine].posY, fontSize, color);

			DrawText(pressEnter.text, (static_cast<int>(screenWidth) - MeasureText(pressEnter.text, pressEnter.fontSize)) / 2, pressEnter.posY, pressEnter.fontSize, YELLOW);
		}

		void deinitialization()
		{

		}
	}
}