#ifndef CREDITS_H
#define CREDITS_H

namespace game
{
	namespace credits
	{
		void initialization();

		void input();

		void update();

		void draw();

		void deinitialization();
	}
}

#endif // CREDITS_H 
