#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "raylib.h"

namespace game
{
	namespace gameplay
	{
		void initialization();

		void input();

		void update();

		void draw();

		void deinitialization();
	}
}

#endif // GAMEPLAY_H
