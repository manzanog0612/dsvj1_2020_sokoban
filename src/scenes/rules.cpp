#include "scenes/rules.h"

#include "raylib.h"

#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "configurations/configurations.h"
#include "resourses/textures.h"
#include "resourses/audio.h"
#include "resourses/events.h"

using namespace game;
using namespace global_vars;
using namespace global_drawing_vars;
using namespace configurations;
using namespace textures;
using namespace audio;
using namespace events;

namespace game
{
	namespace rules
	{
		void initialization()
		{
			loadGameplayTextures();
			loadRulesTextures();
			const short fontSize = static_cast<int>(36.0f * drawScaleY);

			rulesText[firstLine].text = "You're a cat traped in a hunted";
			rulesText[secondLine].text = "house! In order to get out you";
			rulesText[thirdLine].text = "need to take the pumpkins to the";
			rulesText[fourthLine].text = "green tiles without getting stuck.";
			rulesText[fifthLine].text = "Good luck!";

			pressEnter.text = "Press enter to go back to menu";

			rulesText[firstLine].posY = static_cast<int>(screenLimit.down / 3.5f);
			rulesText[secondLine].posY = static_cast<int>(rulesText[firstLine].posY + fontSize * 1.7f);
			rulesText[thirdLine].posY = static_cast<int>(rulesText[secondLine].posY + fontSize * 1.7f);
			rulesText[fourthLine].posY = static_cast<int>(rulesText[thirdLine].posY + fontSize * 1.7f);
			rulesText[fifthLine].posY = static_cast<int>(rulesText[fourthLine].posY + fontSize * 1.7f);
		}

		void input()
		{
			if (IsKeyPressed(KEY_ENTER))
				returnToMenu = true;

			if (IsKeyPressed(KEY_M))
				mute = !mute;
		}

		void update()
		{
			if (returnToMenu)
			{
				currentPlaceInGame = PLACEINGAME::MENU;
				returnToMenu = false;
			}

			if (!mute)
				UpdateMusicStream(menuStream);
		}

		void draw()
		{
			const Color color = WHITE;
			const short fontSize = static_cast<int>(36.0f * drawScaleY);
			float treasuresPosY = rulesText[fourthLine].posY + fontSize * 1.5f;

			pressEnter.fontSize = static_cast<int>(25.0f * drawScaleY);
			pressEnter.posY = static_cast<int>(screenLimit.down - pressEnter.fontSize * 2);

			ClearBackground(BLACK);

			DrawTextureEx(menuBackground.texture, { 0.0f, 0.0f }, 0.0f, 1.0f, WHITE);
			//DrawTextureEx(rulesBackground.texture, { 0.0f, 0.0f }, 0.0f, 1.0f, WHITE);

			DrawTextureRec(catT.texture, {catT.width / 3, 0.0f, catT.width / 3, catT.height / 4 }, { screenWidth / 2 - (catT.width / 3) * 3 , rulesText[firstLine].posY - (catT.height / 4) * 3 }, WHITE);
			DrawTextureRec(pumpkinsT.texture, { pumpkinsT.width / 3, 0.0f, pumpkinsT.width / 3, pumpkinsT.height / 3 }, { screenWidth / 2 - (pumpkinsT.width / 3) / 2 , rulesText[firstLine].posY - (catT.height / 4) * 3 }, WHITE);
			DrawTextureRec(floorT.texture, { 0.0f, 0.0f, floorT.width, floorT.height }, { screenWidth / 2 + (floorT.width) * 2 , rulesText[firstLine].posY - (catT.height / 4) * 3 }, GREEN);

			DrawText(rulesText[firstLine].text, (static_cast<int>(screenWidth)- MeasureText(rulesText[firstLine].text, fontSize))/ 2, rulesText[firstLine].posY, fontSize, color);
			DrawText(rulesText[secondLine].text, (static_cast<int>(screenWidth) - MeasureText(rulesText[secondLine].text, fontSize)) / 2, rulesText[secondLine].posY, fontSize, color);
			DrawText(rulesText[thirdLine].text, (static_cast<int>(screenWidth) - MeasureText(rulesText[thirdLine].text, fontSize)) / 2, rulesText[thirdLine].posY, fontSize, color);
			DrawText(rulesText[fourthLine].text, (static_cast<int>(screenWidth) - MeasureText(rulesText[fourthLine].text, fontSize)) / 2, rulesText[fourthLine].posY, fontSize, color);
			DrawText(rulesText[fifthLine].text, (static_cast<int>(screenWidth) - MeasureText(rulesText[fifthLine].text, fontSize)) / 2, rulesText[fifthLine].posY, fontSize, color);

			DrawText(pressEnter.text, (static_cast<int>(screenWidth) - MeasureText(pressEnter.text, pressEnter.fontSize)) / 2, pressEnter.posY, pressEnter.fontSize, YELLOW);
		}

		void deinitialization()
		{
			
		}
	}
}