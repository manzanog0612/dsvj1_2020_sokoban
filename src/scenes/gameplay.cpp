#include "scenes/gameplay.h"

#include <cmath>
#include <vector>
#include <iostream>

#include "raylib.h"

#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "elements/elements.h"
#include "configurations/configurations.h"
#include "resourses/textures.h"
#include "resourses/audio.h"
#include "resourses/events.h"
#include "sub_scenes/background.h"
#include "sub_scenes/game_objects.h"

using namespace game;
using namespace global_vars;
using namespace global_drawing_vars;
using namespace elements;
using namespace configurations;
using namespace textures;
using namespace audio;
using namespace events;
using namespace background;
using namespace game_objects;

namespace game
{
	namespace gameplay
	{
		bool soundPLayedNL = false;
		
		static void restartGame()
		{
			if (win)
				win = false;
			if (soundPLayed)
				soundPLayed = false;
			if (sound2PLayed)
				sound2PLayed = false;
			if (levelChanged)
				levelChanged = false;

			initializateBackground();

			background::actualLevel = 1;
			updateBackgroundLevel();
		}

		static void drawPauseScreen()
		{
			DrawText(pause.text, (screenLimit.right - MeasureText(pause.text, pause.fontSize)) / 2, pause.posY, pause.fontSize, MAGENTA);

			DrawText(pressEnter.text, (screenLimit.right - MeasureText(pressEnter.text, pressEnter.fontSize)) / 2, pressEnter.posY, pressEnter.fontSize, YELLOW);

			DrawText(pressP.text, (screenLimit.right - MeasureText(pressP.text, pressP.fontSize)) / 2, pressP.posY, pressP.fontSize, ORANGE);

			DrawText(pressM.text, (screenLimit.right - MeasureText(pressM.text, pressM.fontSize)) / 2, pressM.posY, pressM.fontSize, ORANGE);

			DrawText(pressR.text, (screenLimit.right - MeasureText(pressR.text, pressR.fontSize)) / 2, pressR.posY, pressR.fontSize, ORANGE);

			DrawText(pressSpace.text, (screenLimit.right - MeasureText(pressSpace.text, pressR.fontSize)) / 2, pressSpace.posY, pressSpace.fontSize, ORANGE);
		}

		static void drawWinningScreen()
		{
			winner[0].text = "You win";
			winner[1].text = "Congratulations!";
			pressR.text = "Press R to restart the game";
			pressEnter.text = "Press enter to go back to menu";

			winner[0].fontSize = static_cast<int>(55.0f * drawScaleY);
			winner[1].fontSize = static_cast<int>(40.0f * drawScaleY);
			pressEnter.fontSize = static_cast<int>(25.0f * drawScaleY);
			pressR.fontSize = pressEnter.fontSize;

			winner[0].posY = static_cast<int>(screenHeight / 2.0f - winner[0].fontSize / 2.0f);
			winner[1].posY = static_cast<int>(winner[0].posY + winner[0].fontSize + winner[1].fontSize / 4.0f);
			pressEnter.posY = screenLimit.down - pressEnter.fontSize * 2;
			pressR.posY = winner[0].posY - winner[0].fontSize;

			DrawText(winner[0].text, (static_cast<int>(screenWidth) - MeasureText(winner[0].text, winner[0].fontSize)) / 2,
				winner[0].posY, winner[0].fontSize, YELLOW);

			DrawText(winner[1].text, (static_cast<int>(screenWidth) - MeasureText(winner[1].text, winner[1].fontSize)) / 2,
				winner[1].posY, winner[1].fontSize, YELLOW);

			DrawText(pressR.text, (screenLimit.right - MeasureText(pressR.text, pressR.fontSize)) / 2, pressR.posY, pressR.fontSize, MAGENTA);

			DrawText(pressEnter.text, (screenLimit.right - MeasureText(pressEnter.text, pressEnter.fontSize)) / 2, pressEnter.posY, pressEnter.fontSize, YELLOW);
		}

		static void drawNextLevelScreen()
		{
			nextLevelScreen[0].text = "Marvelous! Now next level";
			nextLevelScreen[1].text = "Press E to continue";

			nextLevelScreen[0].fontSize = static_cast<int>(55.0f * drawScaleY);
			nextLevelScreen[1].fontSize = static_cast<int>(40.0f * drawScaleY);

			nextLevelScreen[0].posY = static_cast<int>(screenHeight / 2.0f - nextLevelScreen[0].fontSize / 2.0f);
			nextLevelScreen[1].posY = static_cast<int>(nextLevelScreen[0].posY + nextLevelScreen[0].fontSize + nextLevelScreen[1].fontSize / 4.0f);

			DrawText(nextLevelScreen[0].text, (static_cast<int>(screenWidth) - MeasureText(nextLevelScreen[0].text, nextLevelScreen[0].fontSize)) / 2,
				nextLevelScreen[0].posY, nextLevelScreen[0].fontSize, MAGENTA);

			DrawText(nextLevelScreen[1].text, (static_cast<int>(screenWidth) - MeasureText(nextLevelScreen[1].text, nextLevelScreen[1].fontSize)) / 2,
				nextLevelScreen[1].posY, nextLevelScreen[1].fontSize, MAGENTA);
		}
		
		static void setSoundNextLevel()
		{
			levelCompleted = true;
			soundPLayedNL = true;
		}

		//------------------------------------------------------------------------------

		void initialization()
		{
			pause.text = "Pause";
			pressEnter.text = "Press enter to go back to menu";
			pressP.text = "Press P to coninue";
			pressR.text = "Press R to restart the game";
			pressM.text = "Press M to mute audio";
			pressSpace.text = "Press Space to restart the level";

			pause.fontSize = static_cast<int>(80.0f * drawScaleY);
			pressEnter.fontSize = static_cast<int>(25.0f * drawScaleY);
			pressP.fontSize = pressEnter.fontSize;
			pressR.fontSize = pressEnter.fontSize;
			pressM.fontSize = pressEnter.fontSize;
			pressSpace.fontSize = pressEnter.fontSize;

			pause.posY = static_cast<int>(screenHeight) / 2 - pause.fontSize / 2;
			pressEnter.posY = screenLimit.down - pressEnter.fontSize * 2;
			pressP.posY = pause.posY - pressP.fontSize * 3;
			pressM.posY = pause.posY + pause.fontSize + pressP.fontSize * 2;
			pressR.posY = pressM.posY + pressM.fontSize * 2;
			pressSpace.posY = pause.posY - pause.fontSize * 3;

			restartGame();
			loadGameplayTextures();
			initializePlayerInput();
		}

		void input()
		{
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				mouseDown = true;
			else if (mouseDown)
				mouseDown = false;

			if (IsKeyPressed(KEY_P)) 	
				pauseGame = !pauseGame;

			if (IsKeyPressed(KEY_ENTER))
				returnToMenu = true;

			if (IsKeyPressed(KEY_R))	
				restart = true;

			if (IsKeyPressed(KEY_M))	
				mute = !mute;

			if (IsKeyPressed(KEY_SPACE))
				restartLevel = true;

			if (IsKeyPressed(KEY_E) && levelChanged)
				nextLevel = true;

			setPlayerInput();

			#if DEBUG
			if (IsKeyPressed(KEY_W))
				win = true;

			if (IsKeyPressed(KEY_Q))
				levelChanged = true;
			#endif // DEBUG
		}

		void update()
		{
			if (returnToMenu)
			{
				currentPlaceInGame = PLACEINGAME::MENU;
				returnToMenu = false;
				pauseGame = false;
				restartGame();
			}
			
			if (restart)
			{
				restart = false;
				restartGame();
				SetMusicVolume(gameplayStream, 0.1f);
			}

			if (!win)
			{
				if (!pauseGame)
				{
					updateGameObjects();

					if (game_objects::checkLevelComplete())
						levelChanged = true;

					if (levelChanged)
					{
						if (actualLevel == 5)
						{
							win = true;
						}
						else
						{
							if (!nextLevel)
							{
								drawNextLevelScreen();

								if(!soundPLayedNL)
									setSoundNextLevel();
							}
							else
							{
								background::actualLevel++;
								updateBackgroundLevel();
								levelChanged = false;
								nextLevel = false;
								soundPLayedNL = false;
							}
						}
					}

					if (restartLevel)
					{
						restartLevel = false;
						updateBackgroundLevel();
					}
				}
			}
			else
			{
				SetMusicVolume(gameplayStream, 0.05f);

				if (!soundPLayed)
				{
					winningMatch = true;
					soundPLayed = true;
				}

				if (!IsSoundPlaying(winning) && !winningMatch && !sound2PLayed)
				{
					winningMatch2 = true;
					sound2PLayed = true;
				}
			}

			//audio
			if (!inGameplay)
			{
				inGameplay = true;
				playMusic();
			}
			if (!mute)
			{
				UpdateMusicStream(gameplayStream);
			}
			playSound();
			deactivateEvents();
		}

		void draw()
		{
			ClearBackground(WHITE);

			DrawTextureEx(gameplayBackground.texture, { 0.0f, 0.0f }, 0.0f, 1.0f, WHITE);

			if (!win)
			{
				if (!pauseGame)
				{
					if (levelChanged)
					{
						if (!nextLevel)
							drawNextLevelScreen();
					}
					else
					{
						DrawText(pressSpace.text, (screenLimit.right - MeasureText(pressSpace.text, pressR.fontSize)) / 2, pressSpace.posY, pressSpace.fontSize, ORANGE);
						drawBackground();
					}
				}
				else
				{
					drawPauseScreen();
				}
			}
			else
			{
				drawWinningScreen();
			}
		}

		void deinitialization()
		{

		}
	}
}
