#ifndef GLOBAL_DRAWING_VARS_H
#define GLOBAL_DRAWING_VARS_H

#include "raylib.h"

namespace game
{
	namespace global_drawing_vars
	{
		struct Words
		{
			const char* text;
			int posY;
			int  fontSize;
		};

		extern float originalScreenWidth;
		extern float originalScreenHeight;

		extern float drawScaleX;
		extern float drawScaleY;

		extern Words title;

		extern Words playOp;
		extern Words options;
		extern Words rulesOp;
		extern Words creditsOp;
		extern Words exit;

		extern Words rulesText[8];
		extern Words creditsText[32];

		const short firstLine = 0;
		const short secondLine = 1;
		const short thirdLine = 2;
		const short fourthLine = 3;
		const short fifthLine = 4;
		const short sixthLine = 5;
		const short seventhLine = 6;
		const short eighthLine = 7;
		const short ninethLine = 8;
		const short tenthLine = 9;
		const short eleventhLine = 10;
		const short twelfthLine = 11;
		const short thirteenthLine = 12;
		const short fourteenthLine = 13;
		const short	fifteenthLine = 14;
		const short sixteenthLine = 15;
		const short seventeenthLine = 16;
		const short eighteenth = 17;
		const short nineteenth = 18;
		const short twentieth = 19;
		const short twenty_firstLine = 20;
		const short twenty_secondLine = 21;
		const short twenty_thirdLine = 22;
		const short twenty_fourthLine = 23;
		const short twenty_fifthLine = 24;
		const short twenty_sixthLine = 25;
		const short twenty_seventhLine = 26;
		const short twenty_eighthLine = 27;
		const short twenty_ninethLine = 28;
		const short	thirtiethLine = 29;
		const short	thirty_firstLine = 30;
		const short	thirty_secondLine = 31;

		extern Words pressEnter;
		extern Words pressP;
		extern Words pressR;
		extern Words pressM;
		extern Words pressSpace;

		extern Words score;

		extern Words pause;

		extern Words winner[2];
		extern Words nextLevelScreen[2];

		extern Words youLost;

		extern Words gameVersion;

		extern Words screenResolution[2];
	}
}

#endif //GLOBAL_DRAWING_VARS_H
