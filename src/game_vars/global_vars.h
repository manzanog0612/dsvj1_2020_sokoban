#ifndef GLOBAL_VARS_H
#define GLOBAL_VARS_H

#include "raylib.h"

#include "elements/elements.h"
#include "configurations/configurations.h"

using namespace game;
using namespace elements;
using namespace configurations;

namespace game
{
	namespace global_vars
	{
		const short amountRows = 10;
		const short amountColumns = 18;

		extern short fps;

		extern PLACEINGAME currentPlaceInGame;
		extern PLACEINGAME futurePlaceInGame;

		extern Mouse mouse;

		extern bool menuOptionChosen;

		extern bool returnToMenu;

		extern bool pauseGame;

		extern bool restart;

		extern short lineThickness;

		extern bool win;

		extern short screenResolutionChoice;

		extern bool mouseDown;

		extern Rectangle gameSpace;

		extern bool levelChanged;

		extern bool restartLevel;

		extern bool nextLevel;

		extern bool playingGame;
	}
}
#endif //GLOBAL_VARS_H
