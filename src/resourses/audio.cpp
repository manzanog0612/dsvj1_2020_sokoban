#include "audio.h"

#include "events.h"

using namespace game;
using namespace events;

namespace game
{
	namespace audio
	{
		Sound click;
		Sound enter;
		Sound youWin;
		Sound winning;
		Sound purr;
		Sound pushing;
		Sound nextLevelS;
		Music gameplayStream;
		Music menuStream;

		bool soundPLayed = false;
		bool sound2PLayed = false;
		bool soundMenuPlayed = false;
		
		void loadAudio()
		{
			click = LoadSound("res/assets/audio/sounds/click.mp3");
			enter = LoadSound("res/assets/audio/sounds/enter.mp3");
			youWin = LoadSound("res/assets/audio/sounds/youWin.mp3");
			winning = LoadSound("res/assets/audio/sounds/winning.mp3");
			purr = LoadSound("res/assets/audio/sounds/catMoving.mp3");
			pushing = LoadSound("res/assets/audio/sounds/pumpkinMoving.mp3");
			nextLevelS = LoadSound("res/assets/audio/sounds/nextLevel.mp3");
			menuStream = LoadMusicStream("res/assets/audio/music/menu.mp3");
			gameplayStream = LoadMusicStream("res/assets/audio/music/gameplay.mp3");
		}

		void playSound()
		{
			if (!mute && selectOption)		PlaySoundMulti(click);
			if (!mute && enterPressed)		PlaySoundMulti(enter);
			if (!mute && winningMatch)		PlaySound(winning);
			if (!mute && winningMatch2)		PlaySound(youWin);
			if (!mute && movingCat)			PlaySoundMulti(purr);
			if (!mute && movingPumpkin)		PlaySoundMulti(pushing);
			if (!mute && levelCompleted)	PlaySoundMulti(nextLevelS);

			SetSoundVolume(click, 0.5f);
			SetSoundVolume(enter, 0.5f);
			SetSoundVolume(winning, 0.1f);
			SetSoundVolume(youWin, 0.8f);
			SetSoundVolume(purr, 0.5f);
			SetSoundVolume(pushing, 0.4f);
			SetSoundVolume(nextLevelS, 0.3f);
		}

		void playMusic()
		{
			if (inMenu) PlayMusicStream(menuStream);
			if (inGameplay) PlayMusicStream(gameplayStream);

			SetMusicVolume(menuStream, 0.1f);
			SetMusicVolume(gameplayStream, 0.1f);
		}

	}
}