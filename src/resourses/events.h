#ifndef EVENTS_H
#define EVENTS_H

namespace game
{
	namespace events
	{
		extern bool selectOption;
		extern bool enterPressed;
		extern bool winningMatch;
		extern bool winningMatch2;
		extern bool movingCat;
		extern bool movingPumpkin;
		extern bool levelCompleted;
		extern bool inMenu;
		extern bool inGameplay;
		extern bool mute;
	
		void deactivateEvents();
	}
}

#endif // EVENTS_H