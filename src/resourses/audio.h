#ifndef AUDIO_H
#define AUDIO_H

#include "raylib.h"

namespace game
{
	namespace audio
	{
		extern Sound click;
		extern Sound enter;
		extern Sound youWin;
		extern Sound winning;
		extern Sound purr;
		extern Sound nextLevelS;
		extern Music gameplayStream;
		extern Music menuStream;

		extern bool soundPLayed;
		extern bool sound2PLayed;
		extern bool soundMenuPlayed;

		void loadAudio();

		void playSound();
		void playMusic();
	}
}

#endif // AUDIO_H
