#include "resourses/events.h"

namespace game
{
	namespace events
	{
		bool selectOption = false;
		bool enterPressed = false;
		bool winningMatch = false;
		bool winningMatch2 = false;
		bool movingCat = false;
		bool movingPumpkin = false;
		bool levelCompleted = false;
		bool inMenu = false;
		bool inGameplay = false;
		bool mute = false;

		void deactivateEvents()
		{
			if (winningMatch)		winningMatch = false;
			if (winningMatch2)		winningMatch2 = false;
			if (enterPressed)		enterPressed = false;
			if (movingCat)			movingCat = false;
			if (movingPumpkin)		movingPumpkin = false;
			if (levelCompleted)		levelCompleted = false;
		}
	}
}