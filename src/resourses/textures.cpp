#include "resourses/textures.h"

#include "raylib.h"

#include "configurations/configurations.h"
#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "sub_scenes/background.h"

using namespace configurations;
using namespace global_vars;
using namespace global_drawing_vars;

namespace game
{
	namespace textures
	{
		Textures menuBackground;
		Textures gameplayBackground;
		Textures rulesBackground;
		Textures creditsBackground;
		Textures pauseBackground;
		Textures catT;
		Textures pumpkinsT;
		Textures wallT;
		Textures floorT;
		Textures arrowNext;
		Textures arrowPrevious;
		

		void loadMenuTextures()
		{
			//images
			Image menuBackgroundAux = LoadImage("res/assets/textures/menuBackground.png");

			//images to images with pointer to resize
			Image* menuBackgroundAuxP = &menuBackgroundAux;

			//width

			//height

			//resizing
			ImageResize(menuBackgroundAuxP, static_cast<int>(screenWidth), static_cast<int>(screenHeight));

			//transformation from image* to image 
			menuBackgroundAux = *menuBackgroundAuxP;

			//transformation from image to texture2D
			menuBackground.texture = LoadTextureFromImage(menuBackgroundAux);
		}

		void loadGameplayTextures()
		{
			//images
			Image gameplayBackgroundAux = LoadImage("res/assets/textures/gameplayBackground.png");
			Image pauseBackgroundAux = LoadImage("res/assets/textures/pauseBackground.png");
			Image pumpkinsTAux = LoadImage("res/assets/textures/pumpkins.png");
			Image floorTAux = LoadImage("res/assets/textures/floor.png");			
			Image catTAux = LoadImage("res/assets/textures/cat.png");
			Image wallTAux = LoadImage("res/assets/textures/wall.png");

			//images to images with pointer to resize
			Image* gameplayBackgroundAuxP = &gameplayBackgroundAux;
			Image* pauseBackgroundAuxP = &pauseBackgroundAux;
			Image* pumpkinsTAuxP = &pumpkinsTAux;
			Image* floorTAuxP = &floorTAux;
			Image* catTAuxP = &catTAux;
			Image* wallTAuxP = &wallTAux;
			
			//width
			pumpkinsT.width = background::backgroundMap[0][0].dimentions.width * 3.0f;
			catT.width = background::backgroundMap[0][0].dimentions.width * 3.0f;
			floorT.width = background::backgroundMap[0][0].dimentions.width + 2.0f * drawScaleX;
			wallT.width = background::backgroundMap[0][0].dimentions.width;

			//height
			pumpkinsT.height = background::backgroundMap[0][0].dimentions.height * 3.0f;
			catT.height = background::backgroundMap[0][0].dimentions.height * 4.0f;
			floorT.height = background::backgroundMap[0][0].dimentions.height + 2.0f * drawScaleX;
			wallT.height = background::backgroundMap[0][0].dimentions.height * 2.0f;

			//resizing
			ImageResize(gameplayBackgroundAuxP, static_cast<int>(screenWidth), static_cast<int>(screenHeight));
			ImageResize(pauseBackgroundAuxP, static_cast<int>(screenWidth), static_cast<int>(screenHeight));
			ImageResize(pumpkinsTAuxP, static_cast<int>(pumpkinsT.width), static_cast<int>(pumpkinsT.height));
			ImageResize(floorTAuxP, static_cast<int>(floorT.width), static_cast<int>(floorT.height));
			ImageResize(catTAuxP, static_cast<int>(catT.width), static_cast<int>(catT.height));
			ImageResize(wallTAuxP, static_cast<int>(wallT.width), static_cast<int>(wallT.height));


			//transformation from image* to image 
			gameplayBackgroundAux = *gameplayBackgroundAuxP;
			pauseBackgroundAux = *pauseBackgroundAuxP;
			pumpkinsTAux = *pumpkinsTAuxP;
			floorTAux = *floorTAuxP;
			catTAux = *catTAuxP;
			wallTAux = *wallTAuxP;

			//transformation from image to texture2D
			gameplayBackground.texture = LoadTextureFromImage(gameplayBackgroundAux);
			pauseBackground.texture = LoadTextureFromImage(pauseBackgroundAux);
			pumpkinsT.texture = LoadTextureFromImage(pumpkinsTAux);
			floorT.texture = LoadTextureFromImage(floorTAux);
			catT.texture = LoadTextureFromImage(catTAux);
			wallT.texture = LoadTextureFromImage(wallTAux);
		}

		void loadRulesTextures()
		{
			//images
			Image rulesBackgroundAux = LoadImage("res/assets/textures/rulesBackground.png");

			//images to images with pointer to resize
			Image* rulesBackgroundAuxP = &rulesBackgroundAux;

			//width

			//height

			//resizing
			ImageResize(rulesBackgroundAuxP, static_cast<int>(screenWidth), static_cast<int>(screenHeight));

			//transformation from image* to image 
			rulesBackgroundAux = *rulesBackgroundAuxP;

			//transformation from image to texture2D
			rulesBackground.texture = LoadTextureFromImage(rulesBackgroundAux);
		}

		void loadCreditsTextures()
		{
			//images
			Image creditsBackgroundAux = LoadImage("res/assets/textures/creditsBackground.png");

			//images to images with pointer to resize
			Image* creditsBackgroundAuxP = &creditsBackgroundAux;

			//width

			//height

			//resizing
			ImageResize(creditsBackgroundAuxP, static_cast<int>(screenWidth), static_cast<int>(screenHeight));

			//transformation from image* to image 
			creditsBackgroundAux = *creditsBackgroundAuxP;

			//transformation from image to texture2D
			creditsBackground.texture = LoadTextureFromImage(creditsBackgroundAux);
		}
	}
}