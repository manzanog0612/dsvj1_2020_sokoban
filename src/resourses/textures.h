#ifndef TEXTURES_H
#define TEXTURES_H

#include "raylib.h"

#include "game_vars/global_vars.h"

using namespace global_vars;

namespace game
{
	namespace textures
	{
		struct Textures
		{
			Texture2D texture;
			float width;
			float height;
		};

		extern Textures menuBackground;
		extern Textures gameplayBackground;
		extern Textures rulesBackground;
		extern Textures creditsBackground;
		extern Textures pauseBackground;
		extern Textures catT;
		extern Textures pumpkinsT;
		extern Textures wallT;
		extern Textures floorT;
		extern Textures arrowNext;
		extern Textures arrowPrevious;

		void loadMenuTextures();

		void loadGameplayTextures();

		void loadRulesTextures();

		void loadCreditsTextures();
	}
}

#endif // TEXTURES_H
