#ifndef BACKGROUND_H
#define BACKGROUND_H

#include "game_vars/global_vars.h"

using namespace game;
using namespace global_vars;

namespace game
{
	namespace background
	{
		//W=Wall, F=Floor, C=Cat, P=Pumpkin, E=End, N=Nothing
		enum class B { W, F, C, P, E, N };

		struct Background
		{
			Rectangle dimentions;
			B type;
		};

		extern Background backgroundMap[amountRows][amountColumns];
		extern Background objectsMap[amountRows][amountColumns];
		extern short actualLevel;

		void loadLevel();

		void initializateBackground();

		void updateBackgroundLevel();

		void drawBackground();
	}
}

#endif // BACKGROUND_H
