#ifndef GAME_OBJECTS_H
#define GAME_OBJECTS_H

#include "game_vars/global_vars.h"
#include "elements/elements.h"
#include "background.h"

using namespace game;
using namespace global_vars;
using namespace elements;
using namespace background;

namespace game
{
	namespace game_objects
	{
		extern Player cat;

		extern Rectangle frameRec;

		extern short currentAnimation;

		void initializePlayerInput();

		void setPlayerPlace(short row, short column);

		void setPlayerInput();

		bool checkLevelComplete();

		void restartCatAttributes();

		void updateGameObjects();
	}
}

#endif // GAME_OBJECTS_H