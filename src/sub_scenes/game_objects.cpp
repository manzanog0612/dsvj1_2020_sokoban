#include "game_objects.h"

#include <iostream>

#include "resourses/textures.h"
#include "resourses/events.h"

using namespace game;
using namespace textures;
using namespace events;

namespace game
{
	namespace game_objects
	{
		Player cat;

		bool movementSet = false;

		//animation vars
		int framesCounter = 0;
		int currentFrame = -1;
		Rectangle frameRec;
		short currentAnimation;

		float startPos = 0.0f;
		float endPos = 0.0f;
		float timeLaps = 0.0f;
		bool movementInX = false;
		short catIndex[2] = { 0,0 };

		void initializePlayerInput()
		{
			cat.control.up = KEY_UP;
			cat.control.down = KEY_DOWN;
			cat.control.left = KEY_LEFT;
			cat.control.right = KEY_RIGHT;

			cat.moving = false;
		}

		void setPlayerPlace(short row, short column)
		{
			cat.row = row;
			cat.column = column;
		}

		static void setPumpkingMovingSound()
		{
			if (movingCat)
				movingCat = false;
			movingPumpkin = true;
		}

		static void setCatBlockedSound()
		{
			movingCat = true;
		}

		void setPlayerInput()
		{
			catIndex[0] = cat.row;
			catIndex[1] = cat.column;

			if (cat.moving) return;	

			if (IsKeyPressed(cat.control.up))
			{
				if (objectsMap[cat.row - 1][cat.column].type == B::P)
				{
					if (backgroundMap[cat.row - 2][cat.column].type != B::W && 
						objectsMap[cat.row - 2][cat.column].type != B::P)
						cat.row--;
				}
				else if (backgroundMap[cat.row - 1][cat.column].type != B::W)
					cat.row--;

				currentAnimation = 4;

				movementInX = false;
			}
			else if (IsKeyPressed(cat.control.down))
			{

				if (objectsMap[cat.row + 1][cat.column].type == B::P)
				{
					if (backgroundMap[cat.row + 2][cat.column].type != B::W && 
						objectsMap[cat.row + 2][cat.column].type != B::P)
						cat.row++;
				}
				else if (backgroundMap[cat.row + 1][cat.column].type != B::W)
					cat.row++;

				currentAnimation = 1;

				movementInX = false;
			}
			else if (IsKeyPressed(cat.control.left))
			{
				if (objectsMap[cat.row][cat.column - 1].type == B::P) //check pumpkin
				{
					if (backgroundMap[cat.row][cat.column - 2].type != B::W && // check pumpkin-wall
						objectsMap[cat.row][cat.column - 2].type != B::P) // check pumpkin-pumpkin
						cat.column--;
				}
				else if (backgroundMap[cat.row][cat.column - 1].type != B::W)
					cat.column--;

				currentAnimation = 2;

				movementInX = true;
			}
			else if (IsKeyPressed(cat.control.right))
			{
				if (objectsMap[cat.row][cat.column + 1].type == B::P)
				{
					if (backgroundMap[cat.row][cat.column + 2].type != B::W && 
						objectsMap[cat.row][cat.column + 2].type != B::P)
						cat.column++;
				}
				else if (backgroundMap[cat.row][cat.column + 1].type != B::W)
					cat.column++;

				currentAnimation = 3;

				movementInX = true;
			}

			if (catIndex[0] != cat.row || catIndex[1] != cat.column)
				cat.moving = true;

			if ((IsKeyPressed(cat.control.right) || IsKeyPressed(cat.control.left) ||
				IsKeyPressed(cat.control.up) || IsKeyPressed(cat.control.down)) &&
				!cat.moving)
				setCatBlockedSound();
		}

		static float lerp(float a, float b, float f)
		{
			return (a * (1.0f - f)) + (b * f);
		}

		static void moveCat()
		{
			if (movementInX)
			{
				if (startPos == 0.0f)
					startPos = objectsMap[catIndex[0]][catIndex[1]].dimentions.x;

				if (endPos == 0.0f)
					endPos = objectsMap[cat.row][cat.column].dimentions.x;
			}
			else
			{
				if (startPos == 0.0f)
					startPos = objectsMap[catIndex[0]][catIndex[1]].dimentions.y;

				if (endPos == 0.0f)
					endPos = objectsMap[cat.row][cat.column].dimentions.y;
			}

			DrawTextureRec(catT.texture, frameRec, { cat.pos.x, cat.pos.y }, WHITE);

			if (timeLaps < 1.0f)
			{
				timeLaps += 0.1f * 30.0f * GetFrameTime();
			}

			if (movementInX)
				cat.pos.x = lerp(startPos, endPos, timeLaps);
			else
				cat.pos.y = lerp(startPos, endPos, timeLaps);

			if (timeLaps > 1.0f)
			{
				cat.moving = false;
				timeLaps = 0.0f;
				startPos = 0.0f;
				endPos = 0.0f;
				cat.pos = { backgroundMap[cat.row][cat.column].dimentions.x, backgroundMap[cat.row][cat.column].dimentions.y };				
			}
		}

		static void setCatMovementAnimation(short movement)
		{
			const short spritesPerRow = 3;
			const short spritesPerColumn = 4;
			const short amountOfSprites = spritesPerRow *  2;

			framesCounter++;

			if (framesCounter >= (fps / amountOfSprites))
			{
				framesCounter = 0;
				currentFrame++;

				if (currentFrame > amountOfSprites - 1)
				{
					currentFrame = 0;
				}
							
				if (!cat.moving)
					frameRec.x = static_cast<float>(catT.width / 3.0f);
				else
					frameRec.x = static_cast<float>(currentFrame) * static_cast<float>(catT.width / spritesPerRow);

				switch (movement)
				{
				case 1://down
					frameRec.y = 0.0f;
					break;
				case 2://left
					frameRec.y = static_cast<float>(catT.height / spritesPerColumn);
					break;
				case 3://rigth
					frameRec.y = static_cast<float>(catT.height * 2.0f / spritesPerColumn);
					break;
				case 4://up
					frameRec.y = static_cast<float>(catT.height * 3.0f / spritesPerColumn);
					break;
				default:
					break;
				}

				frameRec.width = catT.width / spritesPerRow;
				frameRec.height = catT.height / spritesPerColumn;
			}
		}

		static void setCatStillTexture(short movement)
		{
			const short spritesPerRow = 3;
			const short spritesPerColumn = 4;

			frameRec.x = static_cast<float>(catT.width / 3.0f);
			
			switch (movement)
			{
			case 1://down
				frameRec.y = 0;
				break;
			case 2://left
				frameRec.y = static_cast<float>(catT.height / spritesPerColumn);
				break;
			case 3://rigth
				frameRec.y = static_cast<float>(catT.height * 2.0f / spritesPerColumn);
				break;
			case 4://up
				frameRec.y = static_cast<float>(catT.height * 3.0f / spritesPerColumn);
				break;
			default:
				break;
			}

			frameRec.width = catT.width / spritesPerRow;
			frameRec.height = catT.height / spritesPerColumn;
		}

		static void movePumpkin()
		{
			if (IsKeyPressed(cat.control.up))
			{
				if (objectsMap[cat.row][cat.column].type == B::P && backgroundMap[cat.row - 1][cat.column].type != B::W)
				{
					objectsMap[cat.row - 1][cat.column].type = B::P;
					setPumpkingMovingSound();					
				}
			}
			if (IsKeyPressed(cat.control.down))
			{
				if (objectsMap[cat.row][cat.column].type == B::P  && backgroundMap[cat.row + 1][cat.column].type != B::W)
				{
					objectsMap[cat.row + 1][cat.column].type = B::P;
					setPumpkingMovingSound();
				}
			}
			if (IsKeyPressed(cat.control.left))
			{
				if (objectsMap[cat.row][cat.column].type == B::P  && backgroundMap[cat.row][cat.column - 1].type != B::W)
				{
					objectsMap[cat.row][cat.column - 1].type = B::P;
					setPumpkingMovingSound();
				}
			}
			if (IsKeyPressed(cat.control.right))
			{
				if (objectsMap[cat.row][cat.column].type == B::P  && backgroundMap[cat.row][cat.column + 1].type != B::W)
				{
					objectsMap[cat.row][cat.column + 1].type = B::P;
					setPumpkingMovingSound();
				}
			}
		}

		static void setMovementCat()
		{
			for (short i = 0; i < amountRows; i++)
			{
				for (short j = 0; j < amountColumns; j++)
				{
					if (objectsMap[i][j].type == B::C && objectsMap[cat.row][cat.column].type != B::C)
					{
						objectsMap[i][j].type = B::N;
						objectsMap[cat.row][cat.column].type = B::C;
						break;
					}
				}
			}

			setCatMovementAnimation(currentAnimation);
		}

		bool checkLevelComplete()
		{
			for (short i = 0; i < amountRows; i++)
			{
				for (short j = 0; j < amountColumns; j++)
				{
					if (objectsMap[i][j].type == B::P)
					{
						if (backgroundMap[i][j].type != B::E)
							return false;
					}
				}
			}

			return true;
		}

		void restartCatAttributes()
		{
			framesCounter = 0;
			currentFrame = -1;
			currentAnimation = 1;
			startPos = 0.0f;
			endPos = 0.0f;
			timeLaps = 0.0f;
			cat.pos = { objectsMap[cat.row][cat.column].dimentions.x, objectsMap[cat.row][cat.column].dimentions.y };
			frameRec.x = static_cast<float>(catT.width / 3.0f);
			frameRec.y = 0;
		}

		void updateGameObjects()
		{
			movePumpkin();
			setMovementCat();

			if (cat.moving)
				moveCat();
		}
	}
}