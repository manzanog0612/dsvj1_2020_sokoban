#include "background.h"

#include <stdlib.h>
#include <iostream>

#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "sub_scenes/game_objects.h"
#include "resourses/textures.h"

using namespace global_vars;
using namespace global_drawing_vars;
using namespace game_objects;
using namespace textures;

namespace game
{
	namespace background
	{
		Background backgroundMap[amountRows][amountColumns];
		Background objectsMap[amountRows][amountColumns];
		short actualLevel = 1;
		int auxDataIndex = 0;
		short levelSize = 360;

		/*static void defineLevels()
		{
			B auxBgMap[amountRows][amountColumns] = { {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W },
													  {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W },
													  {B::W, B::F, B::F, B::F, B::W, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::W, B::F, B::F, B::F, B::W },
													  {B::W, B::F, B::F, B::F, B::W, B::F, B::F, B::F, B::E, B::F, B::F, B::F, B::F, B::W, B::F, B::F, B::F, B::W },
													  {B::W, B::F, B::E, B::F, B::W, B::F, B::F, B::W, B::W, B::W, B::F, B::F, B::F, B::W, B::F, B::E, B::F, B::W },
													  {B::W, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::W },
													  {B::W, B::F, B::E, B::F, B::W, B::F, B::F, B::W, B::W, B::W, B::F, B::F, B::F, B::W, B::F, B::E, B::F, B::W },
													  {B::W, B::F, B::F, B::F, B::W, B::F, B::F, B::F, B::E, B::F, B::F, B::F, B::F, B::W, B::F, B::F, B::F, B::W },
													  {B::W, B::F, B::F, B::F, B::W, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::W, B::F, B::F, B::F, B::W },
													  {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W } };
			
			B auxObMap[amountRows][amountColumns] = { { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													  { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													  { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													  { B::N, B::N, B::P, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::P, B::N, B::N },
												      { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
												      { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::P, B::C, B::P, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
												      { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
												      { B::N, B::N, B::P, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::P, B::N, B::N },
												      { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													  {B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N } };

			

			for (short i = 0; i < amountRows; i++)
			{
				for (short j = 0; j < amountColumns; j++)
				{
					SaveStorageValue(auxDataIndex, static_cast<int>(auxBgMap[i][j]));
					auxDataIndex++;
					SaveStorageValue(auxDataIndex, static_cast<int>(auxObMap[i][j]));
					auxDataIndex++;
				}
			}

			
			//                                                                  ----------------------------------------------------------
			B auxBgMap2[amountRows][amountColumns] = { {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::F, B::W, B::W, B::W, B::W, B::F, B::F, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::F, B::W, B::F, B::F, B::F, B::F, B::F, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::F, B::W, B::F, B::F, B::W, B::W, B::F, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::F, B::W, B::W, B::F, B::W, B::W, B::F, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::F, B::F, B::W, B::W, B::F, B::W, B::W, B::F, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::F, B::F, B::F, B::F, B::F, B::W, B::W, B::F, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::F, B::F, B::W, B::W, B::E, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W } };

			B auxObMap2[amountRows][amountColumns] = { { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::C, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::P, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N } };

			for (short i = 0; i < amountRows; i++)
			{
				for (short j = 0; j < amountColumns; j++)
				{
					SaveStorageValue(auxDataIndex, static_cast<int>(auxBgMap2[i][j]));
					auxDataIndex++;
					SaveStorageValue(auxDataIndex, static_cast<int>(auxObMap2[i][j]));
					auxDataIndex++;
				}
			}

			//                                                                        -----------------------------------------------
			B auxBgMap3[amountRows][amountColumns] = { {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::F, B::F, B::F, B::W, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::E, B::F, B::F, B::F, B::F, B::W, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::F, B::F, B::E, B::W, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::E, B::W, B::W, B::F, B::F, B::W, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::F, B::W, B::F, B::E, B::F, B::W, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::F, B::F, B::E, B::F, B::F, B::E, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::F, B::F, B::F, B::E, B::F, B::F, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W } };

			B auxObMap3[amountRows][amountColumns] = { { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::C, B::P, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::P, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::P, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::P, B::N, B::P, B::P, B::P, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N } };

			for (short i = 0; i < amountRows; i++)
			{
				for (short j = 0; j < amountColumns; j++)
				{
					SaveStorageValue(auxDataIndex, static_cast<int>(auxBgMap3[i][j]));
					auxDataIndex++;
					SaveStorageValue(auxDataIndex, static_cast<int>(auxObMap3[i][j]));
					auxDataIndex++;
				}
			}
			//                                                      ----------------------------------------------------------------------------------
			B auxBgMap4[amountRows][amountColumns] = { {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::F, B::F, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::F, B::F, B::F, B::F, B::F, B::W, B::W, B::F, B::F, B::F, B::E, B::E, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::F, B::F, B::F, B::F, B::F, B::F, B::W, B::W, B::F, B::F, B::E, B::E, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W } };

			B auxObMap4[amountRows][amountColumns] = { { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::C, B::P, B::P, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::P, B::P, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N } };

			for (short i = 0; i < amountRows; i++)
			{
				for (short j = 0; j < amountColumns; j++)
				{
					SaveStorageValue(auxDataIndex, static_cast<int>(auxBgMap4[i][j]));
					auxDataIndex++;
					SaveStorageValue(auxDataIndex, static_cast<int>(auxObMap4[i][j]));
					auxDataIndex++;
				}
			}

			B auxBgMap5[amountRows][amountColumns] = { {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::E, B::E, B::E, B::W, B::E, B::E, B::E, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::E, B::E, B::E, B::E, B::E, B::E, B::E, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::F, B::F, B::F, B::F, B::F, B::F, B::F, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W },
													   {B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W, B::W } };

			B auxObMap5[amountRows][amountColumns] = { { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::C, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::P, B::P, B::P, B::P, B::P, B::P, B::P, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::P, B::P, B::P, B::N, B::P, B::P, B::P, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N },
													   { B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N, B::N } };

			for (short i = 0; i < amountRows; i++)
			{
				for (short j = 0; j < amountColumns; j++)
				{
					SaveStorageValue(auxDataIndex, static_cast<int>(auxBgMap5[i][j]));
					auxDataIndex++;
					SaveStorageValue(auxDataIndex, static_cast<int>(auxObMap5[i][j]));
					auxDataIndex++;
				}
			}
		}*/

		void initializateBackground()
		{
			gameSpace.width = screenWidth / 1.5f;
			gameSpace.height = screenHeight / 1.5f;
			gameSpace.x = (screenWidth - gameSpace.width) / 2.0f;
			gameSpace.y = (screenHeight - gameSpace.height) / 2.0f;

			for (short i = 0; i < amountRows; i++)
			{
				for (short j = 0; j < amountColumns; j++)
				{
					backgroundMap[i][j].dimentions.width = gameSpace.width / amountColumns;
					backgroundMap[i][j].dimentions.height = gameSpace.height / amountRows;
					backgroundMap[i][j].dimentions.x = gameSpace.x + j * (backgroundMap[i][j].dimentions.width);
					backgroundMap[i][j].dimentions.y = gameSpace.y + i * (backgroundMap[i][j].dimentions.height);

					objectsMap[i][j].dimentions.width = backgroundMap[i][j].dimentions.width;
					objectsMap[i][j].dimentions.height = backgroundMap[i][j].dimentions.height;
					objectsMap[i][j].dimentions.x = backgroundMap[i][j].dimentions.x;
					objectsMap[i][j].dimentions.y = backgroundMap[i][j].dimentions.y;
				}
			}

			for (short i = 0; i < amountRows; i++)
			{
				for (short j = 0; j < amountColumns; j++)
				{
					if (objectsMap[i][j].type == B::C)
					{
						setPlayerPlace(i, j);
						cat.pos = { objectsMap[i][j].dimentions.x, objectsMap[i][j].dimentions.y };
						break;
					}
				}
			}
		}

		void updateBackgroundLevel()
		{
			std::cout << auxDataIndex << std::endl;
			auxDataIndex = (actualLevel - 1) * levelSize;
			std::cout << auxDataIndex << std::endl;

			for (short i = 0; i < amountRows; i++)
			{
				for (short j = 0; j < amountColumns; j++)
				{
					backgroundMap[i][j].type = static_cast<B>(LoadStorageValue(auxDataIndex));
					auxDataIndex++;
					objectsMap[i][j].type = static_cast<B>(LoadStorageValue(auxDataIndex));
					auxDataIndex++;
				}
			}

			for (short i = 0; i < amountRows; i++)
			{
				for (short j = 0; j < amountColumns; j++)
				{
					if (objectsMap[i][j].type == B::C)
					{
						setPlayerPlace(i, j);
						cat.pos = { objectsMap[i][j].dimentions.x, objectsMap[i][j].dimentions.y };
						break;
					}
				}
			}

			restartCatAttributes();
		}

		void drawBackground()
		{
			Rectangle auxRec;
			
			DrawRectangleRec(gameSpace, PINK);

			for (short i = 0; i < amountRows; i++)
			{
				for (short j = 0; j < amountColumns; j++)
				{
					switch (backgroundMap[i][j].type)
					{
					case game::background::B::W:

						if (i == 0 || i == amountRows - 1 || backgroundMap[i - 1][j].type != B::W || (backgroundMap[i - 1][j].type == B::W && backgroundMap[i + 1][j].type == B::W))
						{
							auxRec.height = wallT.height / 2.0f;
							auxRec.width = wallT.width;
							auxRec.x = 0.0f;
							auxRec.y = 0.0f;
						}
						else
						{
							auxRec.height = wallT.height / 2.0f;
							auxRec.width = wallT.width;
							auxRec.x = 0.0f;
							auxRec.y = wallT.height / 2.0f;
						}

						DrawTextureRec(wallT.texture, auxRec, { backgroundMap[i][j].dimentions.x, backgroundMap[i][j].dimentions.y }, WHITE);

						break;
					case game::background::B::F:

						DrawTextureEx(floorT.texture, { backgroundMap[i][j].dimentions.x, backgroundMap[i][j].dimentions.y - 1.0f * drawScaleY }, 0.0f, 1.0f, WHITE);

						break;
					case game::background::B::E:
						DrawTextureEx(floorT.texture, { backgroundMap[i][j].dimentions.x, backgroundMap[i][j].dimentions.y - 1.0f * drawScaleY }, 0.0f, 1.0f, GREEN);
						break;
					default:
						break;
					}
				}
			}

			
			for (short i = 0; i < amountRows; i++)
			{
				for (short j = 0; j < amountColumns; j++)
				{
					switch (objectsMap[i][j].type)
					{
					case game::background::B::C:

						DrawTextureRec(catT.texture, frameRec, { cat.pos.x, cat.pos.y }, WHITE);

						break;
					case game::background::B::P:

						auxRec.height = pumpkinsT.height / 3.0f;
						auxRec.width = pumpkinsT.width / 3.0f;
						auxRec.x = pumpkinsT.width / 3.0f;
						auxRec.y = pumpkinsT.height / 3.0f;

						DrawTextureRec(pumpkinsT.texture, auxRec, { objectsMap[i][j].dimentions.x, objectsMap[i][j].dimentions.y }, WHITE);

						break;
					default:
						break;
					}
				}
			}
		}
	}
}