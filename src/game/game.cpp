#include "game.h"

#include <time.h>
#include <stdlib.h>

#include "raylib.h"

#include "scenes/menu.h"
#include "scenes/gameplay.h"
#include "scenes/rules.h"
#include "scenes/credits.h"
#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "configurations/configurations.h"
#include "resourses/textures.h"
#include "resourses/audio.h"

using namespace game;
using namespace global_vars;
using namespace global_drawing_vars;
using namespace configurations;
using namespace textures;
using namespace audio;

namespace game
{
	void initialization()
	{
		title.text = "Sokoban";
		screenWidth = 1024.0f;
		screenHeight = 562.0f;

		InitWindow(static_cast<int>(screenWidth), static_cast<int>(screenHeight), title.text);
		InitAudioDevice();
		SetTargetFPS(fps);

		srand(static_cast<unsigned int>(time(NULL)));

		loadAudio();

		menu::initialization();
		gameplay::initialization();
		rules::initialization();
		credits::initialization();
	}

	void input()
	{
		switch (currentPlaceInGame)
		{
		case PLACEINGAME::MENU:		menu::input();		break;
		case PLACEINGAME::GAMEPLAY:	gameplay::input();	break;
		case PLACEINGAME::RULES:	rules::input();		break;
		case PLACEINGAME::CREDITS:	credits::input();	break;
		case PLACEINGAME::EXIT:
		case PLACEINGAME::NONE:
		default:
			break;
		}
	}

	void update()
	{
		switch (currentPlaceInGame)
		{
		case PLACEINGAME::MENU:		menu::update();		break;
		case PLACEINGAME::GAMEPLAY:	gameplay::update();	break;
		case PLACEINGAME::RULES:	rules::update();	break;
		case PLACEINGAME::CREDITS:	credits::update();	break;
		case PLACEINGAME::EXIT:		playingGame = false;break;
		case PLACEINGAME::NONE:
		default:
			break;
		}
	}

	void draw()
	{
		BeginDrawing();
		switch (currentPlaceInGame)
		{
		case PLACEINGAME::MENU:		menu::draw();		break;
		case PLACEINGAME::GAMEPLAY:	gameplay::draw();	break;
		case PLACEINGAME::RULES:	rules::draw();		break;
		case PLACEINGAME::CREDITS:	credits::draw();	break;
		case PLACEINGAME::EXIT:
		case PLACEINGAME::NONE:
		default:
			break;
		}
		EndDrawing();
	}

	void deinitialization()
	{
		switch (currentPlaceInGame)
		{
		case PLACEINGAME::MENU:		menu::deinitialization();		break;
		case PLACEINGAME::GAMEPLAY:	gameplay::deinitialization();	break;
		case PLACEINGAME::RULES:	rules::deinitialization();		break;
		case PLACEINGAME::CREDITS:	credits::deinitialization();	break;
		case PLACEINGAME::EXIT:
		case PLACEINGAME::NONE:
		default:
			break;
		}
	}

	void run()
	{
		game::initialization();

		while (!WindowShouldClose() && playingGame)
		{
			game::input();
			game::update();
			game::draw();
		}

		CloseWindow();		
	}
}