#ifndef CONFIGURATIONS_H
#define CONFIGURATIONS_H

namespace configurations
{
	struct Mouse
	{
		int posX;
		int posY;
	};

	enum class PLACEINGAME { MENU, GAMEPLAY, RULES, CREDITS, EXIT, NONE };
	enum class LINES { UP, MIDDLE, DOWN };
	enum class IMAGES {ONE, TWO, TRHEE};

	extern float screenWidth;
	extern float screenHeight;

	struct ScreenLimit
	{
		int right = static_cast<int>(screenWidth);
		int left = 0;
		int up = 0;
		int down = static_cast<int>(screenHeight);
	};

	extern ScreenLimit screenLimit;
}

#endif //CONFIGURATIONS_H
